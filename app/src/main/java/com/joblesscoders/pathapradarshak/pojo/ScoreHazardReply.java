package com.joblesscoders.pathapradarshak.pojo;

import java.util.List;

public class ScoreHazardReply {
    private double score;
    private List<Hazards> hazards;

    public ScoreHazardReply(double score, List<Hazards> hazards) {
        this.score = score;
        this.hazards = hazards;
    }

    public double getScore() {
        return score;
    }

    public List<Hazards> getHazards() {
        return hazards;
    }
}
