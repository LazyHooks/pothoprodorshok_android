package com.joblesscoders.pathapradarshak.map;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;
import com.joblesscoders.pathapradarshak.R;
import com.joblesscoders.pathapradarshak.login.HomeActivity;
import com.joblesscoders.pathapradarshak.pojo.Hazards;
import com.joblesscoders.pathapradarshak.pojo.JerkData;
import com.joblesscoders.pathapradarshak.pojo.JerkReply;
import com.joblesscoders.pathapradarshak.pojo.Panic;
import com.joblesscoders.pathapradarshak.pojo.PanicReply;
import com.joblesscoders.pathapradarshak.pojo.ScoreHazardReply;
import com.joblesscoders.pathapradarshak.pojo.User;
import com.joblesscoders.pathapradarshak.posthazard.PostHazardActivity;
import com.joblesscoders.pathapradarshak.utils.RestApiHandler;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, SensorEventListener {
    private static final int LOCATION = 1001;
    private static final int REQUEST_CHECK_SETTINGS = 99 ;
    private static final int ERROR_DIALOG = 22 ;
    private static final int THRESHOLD = 5;
    private Polyline currentPolyline;
    private long lastTimestamp;
    private double lastJerk;
    private SensorManager sensorManager;
    private Sensor sensor;
    private  Marker currentMarker;
    private GoogleMap googleMap;
    private LatLng origin, destination;
    private View navigation,clearbtn1,clearbtn2,addhazards,stop,ll;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;
    private LatLng currentLocation;
    private RestApiHandler restApiHandler;
    private RestApiHandler.RetrofitHandler retrofitHandler;
    private  AutocompleteSupportFragment autocompleteFragment,autocompleteFragment2;
     private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(6.4626999, 68.1097),
            new LatLng(35.513327, 97.39535869999999)
    );
     private List<Marker> markerList = new ArrayList<>();
     private Marker start,end;

    private BottomSheetBehavior sheetBehavior;
    private LinearLayout bottom_sheet;
    private CardView menu_btn_bottom_sheet;
    private CardView panicButton, addHazardButton;
    private boolean isFirst = false;
    private  RestApiHandler.RetrofitHandler retrofitHandlerLocal;
    List<Marker> hazardList = new ArrayList<>();
    private boolean panicMode;
    private String type;
    private boolean isNavigation = false;
    private long currentTimestamp,pastTimestamp = 0;
    private boolean isPolice = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       try {
           type = getIntent().getExtras().getString("type");
       }
       catch (Exception e)
       {
           Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
       }


//        Toast.makeText(this, ""+type, Toast.LENGTH_SHORT).show();
        setContentView(R.layout.activity_main);
        currentTimestamp = new Date().getTime();
        initResources();
        if(type.equals("police")) {
            isPolice = true;
            showPoliceUI();
        }
        else
            showUserUI();

//        initBottomSheet();
        initBottomSheetDialog();
    }

    private void showUserUI() {
        initAccelerometer();
    }

    private void showPoliceUI() {
        navigation.setVisibility(View.GONE);
        ll.setVisibility(View.GONE);
//        addHazardButton.setVisibility(View.GONE);

    }

    private void initBottomSheetDialog() {

        menu_btn_bottom_sheet = findViewById(R.id.menu_btn_bottom_sheet);

        View dialogView = getLayoutInflater().inflate(R.layout.activity_home_bottom_sheet, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(dialogView);

        dialogView.findViewById(R.id.post_new_hazard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PostHazardActivity.class);
                intent.putExtra("latitude", currentLocation.latitude);
                intent.putExtra("longitude", currentLocation.longitude);
                startActivity(intent);
            }
        });


        dialogView.findViewById(R.id.logout_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.logoutUser(MainActivity.this);
                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                MainActivity.this.finish();
            }
        });
        if(isPolice) {
            dialogView.findViewById(R.id.help_layout).setVisibility(View.GONE);
        }
        dialogView.findViewById(R.id.help_me).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MainActivity.this, "help wanted", Toast.LENGTH_SHORT).show();
                displayPanicBottomSheet();
                dialog.dismiss();
            }
        });


        menu_btn_bottom_sheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }

    private void displayPanicBottomSheet() {
        panicMode = true;
        View dialogView = getLayoutInflater().inflate(R.layout.panic_layout, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialogView.findViewById(R.id.stop_location_sharing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                panicMode = false;
            }
        });
        retrofitHandlerLocal.postPanicData(new Panic(currentLocation.latitude,currentLocation.longitude,User.getUser(getApplicationContext()).get_id(),new Date().getTime())).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.code() == 200)
                {
                   // Toast.makeText(MainActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        dialog.show();
    }


    private void initAccelerometer() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }


    private void initResources()
    {
        panicMode = false;
        restApiHandler = new RestApiHandler();
        retrofitHandler = restApiHandler.getRetrofit();
        retrofitHandlerLocal = restApiHandler.getRetrofit2();
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                  //  Toast.makeText(MainActivity.this, locationResult.getLocations().size()+"llo", Toast.LENGTH_SHORT).show();
                    if(currentMarker!=null)
                    {
                      currentMarker.remove();
                    }
                    //


                   currentLocation =new LatLng(location.getLatitude(),location.getLongitude());
                    if(panicMode)
                    {
                        retrofitHandlerLocal.updatePanic(new Panic(currentLocation.latitude,currentLocation.longitude,User.getUser(getApplicationContext()).get_id(),new Date().getTime())).enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                if(response.code() == 200)
                                {
                                    //Toast.makeText(MainActivity.this, "updated "+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {

                            }
                        });
                    }
                    Log.i("helloxDl",location.getLatitude()+" "+location.getLongitude());


                    break;

                }
                Panic user = new Panic(currentLocation.latitude,currentLocation.longitude,User.getUser(getApplicationContext()).get_id());

                retrofitHandlerLocal.updateLocation(user).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                       // if(response.code() == 200)
                       // Toast.makeText(MainActivity.this, ""+response.body().getMessage()+"ll", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                    }
                });


                showCurrentLocation();
//                stopLocationUpdates();
            };
        };

        stop = findViewById(R.id.stop);
        ll = findViewById(R.id.ll);
        stop.setOnClickListener(this);
//        addhazards = findViewById(R.id.addhazards);
        navigation = findViewById(R.id.navigation);
        navigation.setOnClickListener(this);
        clearbtn1 =  findViewById(R.id.clear);
        clearbtn2 =  findViewById(R.id.clear2);
        clearbtn1.setOnClickListener(this);
        clearbtn2.setOnClickListener(this);
//        addhazards.setOnClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.TRANSPARENT);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Window window = this.getWindow();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        // Initialize the AutocompleteSupportFragment.
        autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setHint("From");
        autocompleteFragment.setCountry("IN");
      //  autocompleteFragment.setHasOptionsMenu() = ==

        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                origin = place.getLatLng();
                Log.i("helloxD1", "Place: " + place.getName() + ", " + place.getId());
            }

            @Override
            public void onError(Status status) {

                Log.i("helloxD", "An error occurred: " + status);
            }
        });
        autocompleteFragment.getView().findViewById(R.id.places_autocomplete_clear_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        autocompleteFragment.setText("");
                        view.setVisibility(View.GONE);
                        origin = null;

                    }
                });

         autocompleteFragment2 = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment2);
        autocompleteFragment2.getView().findViewById(R.id.places_autocomplete_clear_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        destination = null;
                        autocompleteFragment2.setText("");
                        view.setVisibility(View.GONE);

                    }
                });

        autocompleteFragment2.setHint("To");
        autocompleteFragment2.setCountry("IN");
        autocompleteFragment2.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG));
        autocompleteFragment2.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
               destination =  place.getLatLng();

                Log.i("helloxD2dest", "Place: " + place.getName() + ", " + place.getId()+place.getLatLng()+" ");
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("helloxD", "An error occurred: " + status);
            }
        });



    }

    private void checkPermissionLocation() {

        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

        if (permissionAccessCoarseLocationApproved) {

            startLocationUpdates();
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION

                    },
                    LOCATION);
        }
    }


    private GeoApiContext getGeoContext() {

        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_maps_key));

    }
    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
         start = mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[0].legs[0].startLocation.lat,results.routes[0].legs[0].startLocation.lng)).title(results.routes[0].legs[0].startAddress));
         end = mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[0].legs[0].endLocation.lat,results.routes[0].legs[0].endLocation.lng)).title(results.routes[0].legs[0].startAddress).snippet(getEndLocationTitle(results)));
    }
    private String getEndLocationTitle(DirectionsResult results){
        return  "Time :"+ results.routes[0].legs[0].duration.humanReadable + " Distance :" + results.routes[0].legs[0].distance.humanReadable;
    }
    private void addPolyline(DirectionsResult results, GoogleMap mMap) {

        List<LatLng> decodedPath = PolyUtil.decode(results.routes[0].overviewPolyline.getEncodedPath());
        retrofitHandlerLocal.getHazardAndScore(decodedPath).enqueue(new Callback<ScoreHazardReply>() {
            @Override
            public void onResponse(Call<ScoreHazardReply> call, Response<ScoreHazardReply> response) {
                if(response.code() == 200)
                {

                    initScoreBottomSheet(response.body());
                    plotHazards(response.body());
//                    Toast.makeText(MainActivity.this, response.body().getScore() + " asuhfiasd", Toast.LENGTH_SHORT).show();
                }
                else{
//                    Toast.makeText(MainActivity.this, "no response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ScoreHazardReply> call, Throwable t) {
//                Toast.makeText(MainActivity.this, t.getMessage() + "Failute", Toast.LENGTH_SHORT).show();
            }
        });
      /*  for(LatLng l: decodedPath)
        {
            CircleOptions circleOptions = new CircleOptions()
                    .center(l)
                    .radius(10)
                    .fillColor(Color.BLACK);
            mMap.addCircle(circleOptions);
            Log.i("hello",l.latitude+" "+l.longitude+" "+decodedPath.size());
        }*/
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(Color.BLACK);
        polyOptions.width(5);
        polyOptions.addAll(decodedPath);
         currentPolyline = mMap.addPolyline(polyOptions);



    }

    private void plotHazards(ScoreHazardReply body) {

        int height = 100;
        int width = 100;

        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.skull);
         bitmap1 = tintImage(bitmap1,R.color.red);
        Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.roadblock2);
        Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.theft);
        Bitmap theft = Bitmap.createScaledBitmap(bitmap3, width, height, false);
        Bitmap roadblock = Bitmap.createScaledBitmap(bitmap2, width, height, false);
        Bitmap accident = Bitmap.createScaledBitmap(bitmap1, width, height, false);



        if(body.getHazards().size()==0)
        {
           // Toast.makeText(this, "No hazard", Toast.LENGTH_SHORT).show();
            return;
        }
//            Toast.makeText(this, "hazards "+body.getHazards().size(), Toast.LENGTH_SHORT).show();
        BitmapDescriptor smallMarkerIcon;
        for(Hazards hazard:body.getHazards())
        {

            if(hazard.getType().equalsIgnoreCase("accident"))
             smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(accident);
            else if(hazard.getType().equalsIgnoreCase("theft"))
                smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(theft);
            else
                smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(roadblock);
            MarkerOptions options = new MarkerOptions()
                    .position(new LatLng(hazard.getLocation().getLatitude(),hazard.getLocation().getLongitude()))
                    .snippet(hazard.getDetails())
                    .title(hazard.getType()).icon(smallMarkerIcon);
           Marker marker = googleMap.addMarker(options);
           hazardList.add(marker);
          //  Toast.makeText(this, "ll"+hazard.getLatitude()+" "+hazard.getLongitude(), Toast.LENGTH_SHORT).show();

        }

    }

    private void initScoreBottomSheet(ScoreHazardReply body) {
        View dialogView = getLayoutInflater().inflate(R.layout.road_score, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(dialogView);

        CardView colorcard = dialogView.findViewById(R.id.score_color_card);
        TextView tv = dialogView.findViewById(R.id.score_textview);
        TextView tvscore = dialogView.findViewById(R.id.score_number);
        tvscore.setText(body.getScore()+"");

        if(body.getScore() < 20) {
            colorcard.setCardBackgroundColor(getResources().getColor(R.color.under100));
        }
        else if(body.getScore() < 40) {
            colorcard.setCardBackgroundColor(getResources().getColor(R.color.under80));
        }
        else if(body.getScore() < 60) {
            colorcard.setCardBackgroundColor(getResources().getColor(R.color.under60));
        }
        else if(body.getScore() < 80) {
            colorcard.setCardBackgroundColor(getResources().getColor(R.color.under40));
        }
        else {
            colorcard.setCardBackgroundColor(getResources().getColor(R.color.under20));
        }
        if(body.getScore() == 0)
            tv.setText("No hazards in your way and the road quality is best!");
        else
            tv.setText("Hazards in your way, drive safely!");

        dialog.show();

    }

    private void showCurrentLocation() {
        CameraPosition cameraPosition;

            final float DEFAULT_ZOOM = 17f;
             cameraPosition = new CameraPosition.Builder()
                    .target(currentLocation)
                    .zoom(DEFAULT_ZOOM)
                    .build();

             CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);

        int height = 100;
        int width = 100;
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.danger);
        Bitmap k = tintImage(b,R.color.red);
        Bitmap smallMarker = Bitmap.createScaledBitmap(k, width, height, false);
        BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);

            MarkerOptions options = new MarkerOptions()
                    .position(currentLocation)
                    .title("Current Location").icon(smallMarkerIcon);
            if(!isPolice)
             showNearbyHazards();

        if(!isFirst||isNavigation) {
            googleMap.animateCamera(cameraUpdate);
            origin = currentLocation;
            autocompleteFragment.setText(getAddressFromLatLng(getApplicationContext(), currentLocation));
            isFirst = true;
        }



    }
    public Bitmap tintImage(Bitmap bitmap, int color) {
        color = 0xfff44336;
        Paint paint = new Paint();
       /* paint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(resource, matrix, paint);
        Paint paint = new Paint();*/

        paint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        Bitmap bitmapResult = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapResult);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return bitmapResult;
    }

    private void showNearbyHazards() {


        BitmapDescriptor icon2;
        icon2 = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
        final MarkerOptions hazardsMarker = new MarkerOptions()
                .icon(icon2);
        pastTimestamp = currentTimestamp;
        currentTimestamp = new Date().getTime();
        if(currentTimestamp-pastTimestamp<=10000)
            return;
        retrofitHandler.getNearby(currentLocation).enqueue(new Callback<ScoreHazardReply>() {
            @Override
            public void onResponse(Call<ScoreHazardReply> call, Response<ScoreHazardReply> response) {
               if(response.code() == 200)
               {
                   plotHazards(response.body());
                   createNotification("New hazard found","The score of the area you're in "+response.body().getScore()+"");
                 /*  for (Marker marker: markerList)
                   {
                       marker.remove();
                   }

                   for(Hazards hazards:response.body().getHazards()) {
                       hazardsMarker.position(new LatLng(hazards.getLocation().getLatitude(),hazards.getLocation().getLongitude()))
                               .title(hazards.getType())
                               .snippet(hazards.getDetails());
                       Marker marker = googleMap.addMarker(hazardsMarker);
                       markerList.add(marker);
                   }

                   Toast.makeText(MainActivity.this, ""+response.body().getHazards().size()+" hazards", Toast.LENGTH_SHORT).show();
                    */
               }
//               else {
//                   Toast.makeText(MainActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
//               }
            }

            @Override
            public void onFailure(Call<ScoreHazardReply> call, Throwable t) {

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        this.googleMap.setLatLngBoundsForCameraTarget(BOUNDS_INDIA);
        this.googleMap.getUiSettings().setZoomControlsEnabled(false);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        if(isPolice)
            getPanicData();

        //plotJerks();

    }

    private void getPanicData() {
        retrofitHandler.getPanicData().enqueue(new Callback<List<PanicReply>>() {
            @Override
            public void onResponse(Call<List<PanicReply>> call, Response<List<PanicReply>> response) {
                if(response.code() == 200)
                {
                    plotPanic(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<PanicReply>> call, Throwable t) {

            }
        });
    }

    private void plotPanic(List<PanicReply> body) {
        Toast.makeText(this, "kll"+body.get(0).getLocation().getLatitude(), Toast.LENGTH_SHORT).show();
        int height = 100;
        int width = 100;
        BitmapDescriptor icon45 = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE);
        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.help);
        //bitmap1 = tintImage(bitmap1,R.color.red);
          Bitmap accident = Bitmap.createScaledBitmap(bitmap1, width, height, false);


        if (body.size() == 0) {
            // Toast.makeText(this, "No hazard", Toast.LENGTH_SHORT).show();
            return;
        }
//            Toast.makeText(this, "hazards "+body.getHazards().size(), Toast.LENGTH_SHORT).show();
        BitmapDescriptor smallMarkerIcon;
        for (PanicReply hazard : body) {


            smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(accident);
            MarkerOptions options = new MarkerOptions()
                    .position(new LatLng(hazard.getLocation().getLatitude(), hazard.getLocation().getLongitude()))
           // Marker marker = new Marker().setPosition()
            //.snippet(hazard.g())
            // .title(hazard.getType())
            .icon(smallMarkerIcon);
             googleMap.addMarker(options);
           // hazardList.add(marker);

        }
    }

    private void plotJerks() {
        BitmapDescriptor icon2;
        icon2 = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE);
        final MarkerOptions hazardsMarker = new MarkerOptions()
                .icon(icon2);

        retrofitHandler.getAllJerkData().enqueue(new Callback<List<JerkReply>>() {
            @Override
            public void onResponse(Call<List<JerkReply>> call, Response<List<JerkReply>> response) {
                if(response.code()==200&&response.body().size()>0)
                {
                    for(JerkReply jerkReply:response.body()) {
                        hazardsMarker.position(new LatLng(jerkReply.getLocation().getLatitude(),jerkReply.getLocation().getLongitude()));
                        googleMap.addMarker(hazardsMarker);
                      //  markerList.add(marker);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<JerkReply>> call, Throwable t) {

            }
        });

    }

    private void setMap() throws InterruptedException, ApiException, IOException {

       // googleMap.clear();
        DateTime now = new DateTime();
        DirectionsResult result = DirectionsApi.newRequest(getGeoContext())
                .mode(TravelMode.DRIVING).origin(new com.google.maps.model.LatLng(origin.latitude,origin.longitude))
                .destination(new com.google.maps.model.LatLng(destination.latitude,destination.longitude)).departureTime(now)
                .await();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * .25);
        LatLngBounds latLngBounds = new LatLngBounds(new LatLng(result.routes[0].bounds.southwest.lat,result.routes[0].bounds.southwest.lng),new LatLng(result.routes[0].bounds.northeast.lat,result.routes[0].bounds.northeast.lng)) ;
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds,width,height,padding));
        addMarkersToMap(result,googleMap);
        addPolyline(result,googleMap);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.navigation:
                startNavigationMode();
            break;


            case R.id.addhazards:
//                Toast.makeText(this, "hazard", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, PostHazardActivity.class);
                intent.putExtra("latitude", currentLocation.latitude);
                intent.putExtra("longitude", currentLocation.longitude);
                startActivity(intent);
                break;
            case R.id.stop:
                stopNavigationMode();
                break;

        }

    }

    private void startNavigationMode() {
        if(origin==null || destination==null) {
//            Toast.makeText(this, "Origin or destination cannot be empty.", Toast.LENGTH_SHORT).show();
            return;

        }
        isNavigation = true;
        navigation.setVisibility(View.GONE);
        stop.setVisibility(View.VISIBLE);
        ll.setVisibility(View.GONE);


        try {
            setMap();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void stopNavigationMode()
    {
        final float DEFAULT_ZOOM = 17f;
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(currentLocation)
                .zoom(DEFAULT_ZOOM)
                .build();

        for(Marker m:hazardList)
            m.remove();


        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.animateCamera(cameraUpdate);
        navigation.setVisibility(View.VISIBLE);
        stop.setVisibility(View.GONE);
        ll.setVisibility(View.VISIBLE);
        currentPolyline.remove();
        start.remove();
        end.remove();
        isNavigation = false;


    }

    @Override
    protected void onResume() {
        super.onResume();
//        if(currentLocation==null)
        if(!isPolice)
        initAccelerometer();
        checkGPS();
        createNotificationChannel();
       // if(currentLocation==null)
       // createNotification("Hello guys!","Nope bye");
        //isFirst = true;
        //createNotification("Hello guys2!","Nope bye");

    }

    private boolean isLocationEnabled() {

        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

       if(permissionAccessCoarseLocationApproved)
           return true;
       else return false;
    }

    private void startLocationUpdates() {

        this.googleMap.setMyLocationEnabled(true);
        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.getMainLooper());

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
        if(!isPolice)
        sensorManager.unregisterListener(this);
    }

    private void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case LOCATION:

                if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION))
                    startLocationUpdates();
//                else
//                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                break;


        }

    }
    private void checkGPS(){

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
       // LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if(isLocationEnabled())
                {

                    startLocationUpdates();

                }
                else {
                    checkPermissionLocation();

                }

            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(MainActivity.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });

    }
    public static String getAddressFromLatLng(Context context, LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            return addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        double x,y,z;
        long ts = new Date().getTime();
        x = event.values[0] * event.values[0];
        y = event.values[1] * event.values[1];
        z = event.values[2] * event.values[2];

        double res = Math.sqrt(x + y + z);


        if(lastJerk > THRESHOLD && ts - lastTimestamp < 1000) {
            return;
        }

        if(res > THRESHOLD) {
            // Jerk detected
            // Alert the server with the location
            reportJerk((int) res);
        }

        lastTimestamp = ts;

        lastJerk = res;
    }

    public void reportJerk(int res) {
        if(currentLocation == null) return;
        JerkData jerkData = new JerkData(res,currentLocation.latitude,currentLocation.longitude,new Date().getTime()) ;
        retrofitHandler.postJerk(jerkData).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.code() == 200)
                {
                    //Toast.makeText(MainActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        // TODO: include retrofit and share the location with jerk value

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    public void createNotification(String title,String message)
    {
        String CHANNEL_ID = "jihkihkihki";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.noti)
                .setColor(getResources().getColor(R.color.black))
                .setContentTitle(title)
                .setContentText(message)
                /*.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(title))*/
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.noti));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        int notificationId = 22;
// notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId, builder.build());
       // Toast.makeText(this, "kl", Toast.LENGTH_SHORT).show();

    }
    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "jihkihkihki";
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name,
                    importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviours after this
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


}
