package com.joblesscoders.pathapradarshak.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.joblesscoders.pathapradarshak.R;
import com.joblesscoders.pathapradarshak.map.MainActivity;
import com.joblesscoders.pathapradarshak.pojo.User;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private View civilian,police;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        civilian = findViewById(R.id.civilian);
        police = findViewById(R.id.police);
        civilian.setOnClickListener(this);
        police.setOnClickListener(this);
        boolean b = User.isNewUser(getApplicationContext());
        Toast.makeText(this, b+" ll", Toast.LENGTH_SHORT).show();
        Log.i("kkl",b+"ll");
        if (!b)
        {
            Intent intent = new Intent(HomeActivity.this, MainActivity.class);
            intent.putExtra("type",User.getUser(getApplicationContext()).getType());
            Toast.makeText(this, ""+User.getUser(getApplicationContext()).getType()+" kk", Toast.LENGTH_SHORT).show();
            startActivity(intent);
           // finish();
         }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.civilian:
                Intent intent = new Intent(HomeActivity.this,LoginActivity.class);
                intent.putExtra("type","user");
                startActivity(intent);
                finish();
                break;
            case R.id.police:
                Intent intent2 = new Intent(HomeActivity.this,LoginActivity.class);
                intent2.putExtra("type","police");
                startActivity(intent2);
                finish();
                break;
        }
    }
}
